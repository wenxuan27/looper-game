# looper-game
A hypercasual mobile game made in Unity3D using C#.

## Showcase:
Check it out at https://looper.bumpystudio.xyz/

## Build
To build this project from source, you need the Unity3D editor version 2018.3.0f2. If you import this project in from the Unity Hub, it should automatically detect and help you download the correct version. 
You can also choose to build the project into a newer version that can run in a newer version of the Unity3D Editor. You might need to rebuild and compile some of the files to do so, tho it is not recommended to do so.

## Releases
https://gitlab.com/wenxuan27/looper-game/-/releases
