﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameObjectOverTime : MonoBehaviour
{
    
    private float lifetime = 20f;
    private float DfromPlayer;

    private PlayerController player;

    public float DY = 10;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;

        

        if (DfromPlayer > DY)
        {
           
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        DfromPlayer = player.transform.position.y - gameObject.transform.position.y;
    }
}