﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerp : MonoBehaviour
{
    public float smooth = 2;
    public Color newColor;

    private SpriteRenderer SR;

    [Range(0f, 1f)]
    public float fadeToBlueAmount = 0f;

    [Range(0f, 1f)]
    public float fadeToGreenAmount = 0f;

    [Range(0f, 1f)]
    public float fadeToRedAmount = 0f;


    // Variable to hold fading speed
    public float fadingSpeed = 100f;

        
    private void Awake()
    {

        SR = GetComponent<SpriteRenderer>();

        
        //newColor = SR.color;

        newColor.r = fadeToRedAmount;
        newColor.g = fadeToGreenAmount;
        newColor.b = fadeToBlueAmount;

        // Set sprite colors
        

        StartFadeToBlue();


    }

    private void Start()
    {
        SR.color = Color.Lerp(Color.green, Color.blue, fadingSpeed);
    }

    private void Update()
    {
        SR.color = Color.Lerp(Color.green, Color.blue, fadingSpeed);


        ColourChanging();

        if (SR.color == Color.blue)
        {
            SR.color = Color.Lerp(Color.blue, Color.green, fadingSpeed);
            OnWait(5f);
        }

        if(SR.color ==Color.green )
        {
            SR.color = Color.Lerp(this.GetComponent<SpriteRenderer>().color, Color.yellow, fadingSpeed);
            OnWait(5f);
        }

        if (SR.color == Color.yellow)
        {
            SR.color = Color.Lerp(Color.yellow, Color.black, fadingSpeed);
            OnWait(5f);
        }

        if (SR.color == Color.black)
        {
            SR.color = Color.Lerp(Color.black, Color.blue, fadingSpeed);
            OnWait(5f);
        }
        
    }

    private void ColourChanging()
    {
        Color colorA = Color.red;
        Color colorB = Color.green;
    }


    IEnumerator FadeToBlue()
    {

        // Loop that runs from 1 down to desirable Blue Channel Color amount
        for (float i = 1f; i >= fadeToBlueAmount; i -= 0.05f)
        {

            // Getting access to Color options
            Color c = SR.material.color;

            // Setting values for Red and Green channels
            c.r = i;
            c.g = i;

            // Set color to Sprite Renderer
            SR.material.color = c;

            // Pause to make color be changed slowly
            yield return new WaitForSeconds(fadingSpeed);
        }
    }

    // Method that starts fading coroutine when UI button is pressed
    public void StartFadeToBlue()
    {
        StartCoroutine("FadeToBlue");
    }

    IEnumerator OnWait(float sec)
    {
        yield return new WaitForSeconds(sec);
    }
}
