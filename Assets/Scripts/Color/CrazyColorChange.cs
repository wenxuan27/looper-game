﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class CrazyColorChange : MonoBehaviour
{

    private SpriteRenderer playerSR;
    public bool isCrazyColorChanging;

    private void Awake()
    {
        playerSR = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isCrazyColorChanging)
        {
            if (playerSR != null)
            {

                Color newColor = new Color(Random.value, Random.value, Random.value, playerSR.color.a);


                playerSR.color = newColor;
            }
        }
    }
}
