﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomColorLerp : MonoBehaviour
{

    [Header("Color.lerp references")]
    [SerializeField] private CanvasGroup ColorGroup;
    [SerializeField] private Image ColorImage;


    [Header("Start Color")]
    [SerializeField] private bool startlerpFade = true;
    [SerializeField] private float startlerpTime = 1.5f;
    [SerializeField] public Color startColor = Color.yellow;
    public Color finishColor = Color.blue;


    private SpriteRenderer SR;


   public Color sColor;
    public Color eColor;
 float duration = 5f; // duration in seconds
 
 private float t = 0; // lerp control variable


    public float every = 7f;   //The public variable "every" refers to "Lerp the color every X"
    float colorstep;
    public Color[] colors = new Color[4]; //Insert how many colors you want to lerp between here, hard coded to 4
    int i;
    Color lerpedColor = Color.red;

    public GameManager GM;



    private void Awake()
    {
        SR = GetComponent<SpriteRenderer>();

        GM = FindObjectOfType<GameManager>();

       // UpdateColor(5f, null);

        //SR.color = Color.Lerp(startColor, finishColor, 5f);

    }

    private void Start()
    {
        //SR.color = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));

        //  FadeOut(5f, Color.green,"red", null);

        colors[0] = Color.red;
        colors[1] = Color.yellow;
        colors[2] = Color.cyan;
        colors[3] = Color.red;


    }
    private void Update()
    {
        
        if (GM.T < 5)
        { // while t below the end limit...

            //SR.color = Color.Lerp(sColor, eColor, t);
           SR.color = Color.Lerp(colors[i], colors[i + 1], colorstep);
            // increment it at the desired rate every update:
            t += Time.deltaTime / duration;
        }
        else if(GM.T < 10)
        {
            
        }
        
        if (colorstep < every)
        { //As long as the step is less than "every"
            lerpedColor = Color.Lerp(colors[i], colors[i + 1], colorstep);
            SR.color= lerpedColor;
            colorstep += 0.025f;  //The lower this is, the smoother the transition, set it yourself
        }
        else
        { //Once the step equals the time we want to wait for the color, increment to lerp to the next color

            colorstep = 0;

            if (i < (colors.Length - 2))
            { //Keep incrementing until i + 1 equals the Lengh
                i++;
            }
            else
            { //and then reset to zero
                i = 0;
            }
            
        }
    }

    private IEnumerator UpdateColor(float transitionTime, Action func)
    {
        SR.color = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));

        yield return new WaitForSeconds(1f);
        

        func?.Invoke();
    }


    public void FadeOut(float transitionTime)
    {
        FadeOut(transitionTime, SR.color, null);
    }

    public void FadeOut(float transitionTime, Color fadeColor, string c)
    {
        FadeOut(transitionTime, fadeColor, c, null);
    }

    public void FadeOut(float transitionTime, Color fadeColor, string c, Action func)
    {
        SR.color = fadeColor;
        StartCoroutine(UpdateFadeRed(transitionTime, c, func));
    }

    private IEnumerator UpdateFadeRed(float transitionTime, string color, Action func)
    {
        float t = 0.0f;

        for (t = 0.0f; t <= 1; t += Time.deltaTime / transitionTime)
        {
            if (color == "red")
            {

                //SR.color = new Color(t, SR.color.g, SR.color.b);

                SR.color = Color.Lerp(Color.red, Color.blue, t);

            }

            if (color == "green")
            {
                SR.color = new Color(0, t, 0);
            }

            if(color == "blue")
            {
                SR.color = new Color(0, 0, t);
            }
            yield return null;
                

        }

        //SR.color.a = 1;

        func?.Invoke();
    }


    /*
    private IEnumerator UpdateFadeRed(float transitionTime, Action func)
    {
        float t = 0.0f;

        for (t = 0.0f; t <= 1; t += Time.deltaTime / transitionTime)
        {
            SR.color = new Color(t,0,0);
            yield return null;

        }

        //SR.color.a = 1;

        func?.Invoke();
    }
    */

}
