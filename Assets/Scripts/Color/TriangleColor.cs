﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleColor : MonoBehaviour
{

    private SpriteRenderer SR;

    float duration = 5f; // duration in seconds
     
    float colorstep;
    public Color[] colors = new Color[4]; //Insert how many colors you want to lerp between here, hard coded to 4
    int i;
    Color lerpedColor = Color.red;

    public GameManager GM;

    public float N = 1;
    public float colorduration = 20;

    public int n = 0;


   
    void Start()
    {

        
        SR = GetComponent<SpriteRenderer>();

        GM = FindObjectOfType<GameManager>();
    }

  
    void Update()
    {

        if (GM.CyclicT < N * colorduration)
        { //As long as the step is less than "every"
            lerpedColor = Color.Lerp(colors[n], colors[n + 1], colorstep);
            SR.color = lerpedColor;
            colorstep += 0.001f;  //The lower this is, the smoother the transition, set it yourself
        }
        else
        {
            N++;
            n++;
        
        }

    }
}
