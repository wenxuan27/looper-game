﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGlitch : MonoBehaviour
{
    private GameCounter GC;
    private GlitchEffect GE;
    private GlitchFx GF;
    private AudioManager AM;

    private AnalogGlitch AG;

    public int playedTimes;


    [SerializeField] private float n1 = 0.25f;
    [SerializeField] private float n2 = 0.6f;
    [SerializeField] private float n3 = 1f;
    [SerializeField] private float n4 = 1.5f;
    [SerializeField] private float n5 = 2f;
    [SerializeField] private float n6 = 3f;
    [SerializeField] private float n7 = 5f;

    public int nUntilEffects = 10;

    public int NT;

    public bool CGT;

    // Start is called before the first frame update
    void Start()
    {
        GC = FindObjectOfType<GameCounter>();

        GE = FindObjectOfType<GlitchEffect>();

        GF = FindObjectOfType<GlitchFx>();

        AG = FindObjectOfType<AnalogGlitch>();

        AM = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {

        playedTimes = GC.playedGameTimes;


        if(playedTimes < nUntilEffects)
        {
            GE.intensity = 0;
            GE.flipIntensity = 0;
            GE.colorIntensity = 0;
            //GF.intensity = 0;

            AG.scanLineJitter = 0f;
            AG.verticalJump = 0f;
            AG.horizontalShake = 0f;
            AG.colorDrift = 0f;
            Debug.Log("1 playedTimes = " + playedTimes);

            AM.nTheme = 0;
            NT = 0;

            CGT = false;
        }
        else if(playedTimes < nUntilEffects + 5)
        {
            GE.intensity = n1;
            GE.flipIntensity = n1;
            GE.colorIntensity = n1;
            //GF.intensity = 0;

            AG.scanLineJitter = 0.01f;
            AG.verticalJump = 0.01f;
            AG.horizontalShake = 0.01f;
            AG.colorDrift = 0.01f;

            Debug.Log("2 playedTimes = " + playedTimes);

            AM.nTheme = 1;
            NT = 1;

            CGT = true;
        }
        else if (playedTimes < nUntilEffects + 10)
        {
            GE.intensity = n2;
            GE.flipIntensity = n2;
            GE.colorIntensity = n2;
            //GF.intensity = 0.0005f;

            AG.scanLineJitter = 0.05f;
            AG.verticalJump = 0.05f;
            AG.horizontalShake = 0.05f;
            AG.colorDrift = 0.1f;

            AM.nTheme = 1;
            NT = 1;
            CGT = true;
        }
        else if (playedTimes < nUntilEffects + 15)
        {
            GE.intensity = n3;
            GE.flipIntensity = n3;
            GE.colorIntensity = n3;
            //GF.intensity = 0.01f;

            AG.scanLineJitter = 0.1f;
            AG.verticalJump = 0.1f;
            AG.horizontalShake = 0.1f;
            AG.colorDrift = 0.2f;

            AM.nTheme = 2;
            NT = 2;
            CGT = true;
        }
        else if (playedTimes < nUntilEffects + 19)
        {
            GE.intensity = n4;
            GE.flipIntensity = n4;
            GE.colorIntensity = n4;
            //GF.intensity = 0.05f;

            AG.scanLineJitter = 0.1f;
            AG.verticalJump = 0.1f;
            AG.horizontalShake = 0.1f;
            AG.colorDrift = 0.3f;

            AM.nTheme = 2;
            NT = 2;
            CGT = true;
        }
        else if (playedTimes < nUntilEffects+ 22)
        {
            GE.intensity = n5;
            GE.flipIntensity = n5;
            GE.colorIntensity = n5;
           // GF.intensity = 0.2f;

            AG.scanLineJitter = 0.01f;
            AG.verticalJump = 0.01f;
            AG.horizontalShake = 0.01f;
            AG.colorDrift = 0.5f;

            AM.nTheme = 2;
            NT = 2;
            CGT = true;
        }
        else if (playedTimes < nUntilEffects + 24)
        {
            GE.intensity = n6;
            GE.flipIntensity = n6;
            GE.colorIntensity = n6;
           // GF.intensity = 0.3f;

            AG.scanLineJitter = 0.5f;
            AG.verticalJump = 0.5f;
            AG.horizontalShake = 0.5f;
            AG.colorDrift = 1f;

            AM.nTheme = 3;
            NT = 3;
            CGT = true;
        }
        else if (playedTimes >= nUntilEffects + 25)
        {
            GE.intensity = n7;
            GE.flipIntensity = n7;
            GE.colorIntensity = n7;
            //GF.intensity = 0.5f;

            AG.scanLineJitter = 1f;
            AG.verticalJump = 1f;
            AG.horizontalShake = 1f;
            AG.colorDrift = 2f;

            AM.nTheme = 3;
            NT = 3;
            CGT = true;
        }

        Debug.Log("CGT" + CGT);
    }
}
