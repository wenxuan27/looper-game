﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //public Transform Player;
    //public Vector3 Offset;



    //private PlayerController player;

    public float fallSpeed = 1;

    public bool isMoving;

    public float xOffset;
    public float yOffset;



    // Start is called before the first frame update
    void Start()
    {
        

        isMoving = true;
    }

    void Update()
    {
        if (isMoving)
            transform.position = new Vector3(xOffset, yOffset, transform.position.z);

        

    }

}
