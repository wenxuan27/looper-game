﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FlashingTMPro : MonoBehaviour
{
    

    public TextMeshProUGUI textMesh;
    public float animSpeedInSec = 1f;
    bool keepAnimating = false;
    public float transitionTime = 5f;
    private bool playerIsFlashing = true;

    private GameManager GM;

    private float T;

    private IEnumerator anim()
    {

        Color currentColor = textMesh.color;

        Color invisibleColor = textMesh.color;
        invisibleColor.a = 0; //Set Alpha to 0

        float oldAnimSpeedInSec = animSpeedInSec;
        float counter = 0;
        while (keepAnimating)
        {
            //Hide Slowly
            while (counter < oldAnimSpeedInSec)
            {
                if (!keepAnimating)
                {
                    yield break;
                }

                //Reset counter when Speed is changed
                if (oldAnimSpeedInSec != animSpeedInSec)
                {
                    counter = 0;
                    oldAnimSpeedInSec = animSpeedInSec;
                }

                counter += Time.deltaTime;
                textMesh.color = Color.Lerp(currentColor, invisibleColor, counter / oldAnimSpeedInSec);
                yield return null;
            }

            yield return null;


            //Show Slowly
            while (counter > 0)
            {
                if (!keepAnimating)
                {
                    yield break;
                }

                //Reset counter when Speed is changed
                if (oldAnimSpeedInSec != animSpeedInSec)
                {
                    counter = 0;
                    oldAnimSpeedInSec = animSpeedInSec;
                }

                counter -= Time.deltaTime;
                textMesh.color = Color.Lerp(currentColor, invisibleColor, counter / oldAnimSpeedInSec);
                yield return null;
            }
        }
    }

    private IEnumerator animnonstop()
    {

        Color currentColor = textMesh.color;

        Color invisibleColor = textMesh.color;
        invisibleColor.a = 0; //Set Alpha to 0

        float oldAnimSpeedInSec = animSpeedInSec;
        float counter = 0;

        float t = 0.0f;

       


            //SR.color = new Color(t, SR.color.g, SR.color.b);
            for (t = 0.0f; t <= 1; t += Time.deltaTime / transitionTime)
            {
                textMesh.alpha = 1-t;
                yield return null;

            }



        

        counter += Time.deltaTime;
                textMesh.color = Color.Lerp(currentColor, invisibleColor, Mathf.Cos(counter));
        yield return null;


    }

    //Call to Start animation
    public void startTextMeshAnimation()
    {
        if (keepAnimating)
        {
            return;
        }
        keepAnimating = true;
        StartCoroutine(anim());
    }

    void startCosanim()
    {
        StartCoroutine(animnonstop());
    }

    private void Start()
    {
        GM = FindObjectOfType<GameManager>();
    }

    

    private void Update()
    {
        T += Time.deltaTime;

        if (playerIsFlashing)
        {
            FlashText();
        }
    }


    private void FlashText()
    {
        float a = textMesh.alpha;
        a = (0.5f * Mathf.Cos(2*T) + 0.5f);
        textMesh.alpha = a;
    
    }
}
