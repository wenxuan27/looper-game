﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public bool gameHasEnded = false;

    public float restartDelay = 1;

    public GameObject DeathMenu;

    private SFXManager SFXM;

    private PauseMenu PM;

    public GameObject playerObject;
    private PlayerController player;

    public GameObject Camera;
    private CameraMovement camF;

    public GameObject BG3;
    private CameraMovement BG3F;

    public float moveSpeed = 10f;

    public float accel = 0.01f;
    public float n;

    public float DistanceY;

    public bool paused;

    public float T;
    public float CyclicT;

    private GameObject start;

    public static float moveSpeedinitial = 2f;

    public bool Turbo;

    public float TurboBoostMult = 10f;

    public AudioManager AM;

    private GameCounter GC;

    private void Start()
    {
        SFXM = FindObjectOfType<SFXManager>();

        PM = FindObjectOfType<PauseMenu>();



      camF = Camera.GetComponent<CameraMovement>();

        BG3F = BG3.GetComponent<CameraMovement>();

        player = playerObject.GetComponent<PlayerController>();



        start = GameObject.FindWithTag("Start");

        AM = FindObjectOfType<AudioManager>();

        GC = FindObjectOfType<GameCounter>();
        
    }

    private void Update()
    {


        if (!paused)
        {
            if (CyclicT < 10 * 20)
            {
                CyclicT += Time.deltaTime;
            }
            else
            {
                CyclicT = 0;

                Debug.Log("restart cycle");
            }

            DistanceY = player.transform.position.y - start.transform.position.y;

            T += Time.deltaTime;

            AM.GetComponent<AudioSource>().pitch = 1 + DistanceY * 0.0001f;

            moveSpeed = Mathf.Sqrt(Mathf.Pow(moveSpeedinitial, 2) + 2 * accel * DistanceY);

            if (Turbo)
            {
                player.fallSpeed = moveSpeed * TurboBoostMult;
                camF.fallSpeed = moveSpeed * TurboBoostMult;
                BG3F.fallSpeed = moveSpeed * TurboBoostMult;
            }
            else
            {
                player.fallSpeed = moveSpeed;
                camF.fallSpeed = moveSpeed;

                BG3F.fallSpeed = moveSpeed;

            }

            

            //player.fallAcc = moveSpeed * n * 0; // if the game works without this then just delete this line


        }
    }

    public void EndGame()
    {
        if(gameHasEnded == false)
        {
            gameHasEnded = true;

            FindObjectOfType<PauseMenu>().endGame();

            //Invoke("Restart", restartDelay);
            DeathMenu.SetActive(true);

            PM.endGame();

            player.rb.velocity = Vector3.zero;

            GC.CyclicplayedGameTimes++;
            
            
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        PM.RestartGame();


        GC.playedGameTimes++;
    }

    public void PlayUISFX()
    {
        SFXM.Play("UI Click");
    }

    public void PlaySound(string soundName)
    {
        SFXM.Play(soundName);
    }


    public void ChangeMoveTo1()
    {
        player.ChangeMoveTo1();
    }

    public void ChangeMoveTo2()
    {
        player.ChangeMoveTo2();
    }

}
