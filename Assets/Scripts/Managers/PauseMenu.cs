﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;



    public GameObject pauseMenuUI;

    public string menuSceneName;

    public AudioMixerSnapshot paused;
    public AudioMixerSnapshot playing;

    Canvas canvas;

    public AudioManager AM;

    public GameObject player;

    private PlayerController playerScript;

    private GameManager GM;

    private void Start()
    {
        canvas = GetComponent<Canvas>();
        Lowpass();
        AM = FindObjectOfType<AudioManager>();

        playerScript = player.GetComponent<PlayerController>();

        GM = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }

        player = GameObject.FindWithTag("Player");
    }

    public void endGame()
    {
        Time.timeScale = 0f;
        GameIsPaused = true;
        Lowpass();
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
        Lowpass();
    }

    public void PauseGame()
    {
        if (GameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Lowpass();


        StartCoroutine(onPause());
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Lowpass();

        playerScript.isPaused = true;

        GM.paused = true;
    }

    private void Lowpass()
    {
        if (GameIsPaused)
        {
            paused.TransitionTo(0.01f);
        }
        else
        {
            playing.TransitionTo(0.01f);
        }
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(menuSceneName);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = true;
        Destroy(AM);
    }

    public void QuitGame()
    {

        Application.Quit();
    }

    public void SetisPaused(bool b)
    {
        GameIsPaused = b;
        Debug.Log(GameIsPaused);
    }

    IEnumerator onPause()
    {
        yield return new WaitForSeconds(0.01f);

        playerScript.isPaused = false;

        GM.paused = false;
    }
}
