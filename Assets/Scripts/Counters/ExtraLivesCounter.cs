﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExtraLivesCounter : MonoBehaviour
{
    public TextMeshProUGUI text;
    private PlayerController player;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        player = FindObjectOfType<PlayerController>();
    }

    void Update()
    {

        text.text = "" + PlayerPrefs.GetInt("ExtraLivesScore");
        Debug.Log("N" + PlayerPrefs.GetInt("ExtraLivesScore"));
    }
}
