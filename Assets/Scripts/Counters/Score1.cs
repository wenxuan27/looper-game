﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Score1 : MonoBehaviour
{
    public TextMeshProUGUI text;
    private PlayerController player;

    public float fscore;
    public int score1;

    private GameManager GM;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        player = FindObjectOfType<PlayerController>();

        GM = FindObjectOfType<GameManager>();

    }

    void Update()
    {
        fscore = GM.DistanceY/5;

        

        score1 = Convert.ToInt32(Mathf.Floor(fscore));

        //Debug.Log(score1);
        
        if (score1 >= 0)
            text.text = "" + score1;



    }

    


}