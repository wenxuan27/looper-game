﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaclescube : MonoBehaviour
{
    public float xDistance = 10;
    public float minSpread = 5;
    public float maxSpread = 10;

    public Transform playerTransform;
    public Transform obstaclePrefab;

    float xSpread;
    float lastXPos;

    void Start()
    {
        lastXPos = Mathf.NegativeInfinity;
        xSpread = Random.Range(minSpread, maxSpread);
    }

    void Update()
    {
        if (playerTransform.position.x - lastXPos >= xSpread)
        {
            float lanePos = Random.Range(0, 3);
            lanePos = (lanePos - 1) * 1.5f;
            Instantiate(obstaclePrefab, new Vector3(playerTransform.position.x + xDistance, lanePos, 0), Quaternion.identity);

            lastXPos = playerTransform.position.x;
            xSpread = Random.Range(minSpread, maxSpread);

            Debug.Log("Triangle");
        }
    }
}
