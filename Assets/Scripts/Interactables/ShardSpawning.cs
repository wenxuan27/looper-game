﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardSpawning : MonoBehaviour
{

    private GameObject player;

    public GameObject Shard;
    public float amountOfShards;

    public float minX, maxX;



    void Start()
    {
        player = GameObject.FindWithTag("Player");

        

        for (int i = 0; i < amountOfShards; i++)
        {
            float xAXIS, yAXIS;
            xAXIS = Random.Range(minX, maxX);

            yAXIS = Random.Range(player.transform.localPosition.y + 10, player.transform.localPosition.y + 50);

            Vector2 pos = new Vector2(xAXIS, yAXIS);
            Instantiate(Shard.transform, pos, Quaternion.identity);
        }
    }

    public bool randomBoolean()
    {
        return (Random.Range(0f, 1f) >= 0.5f);
    }


}
