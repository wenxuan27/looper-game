﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpInvicible : MonoBehaviour
{

    public float duration = 3f;

    private GameObject InvicibleParticleEffect;
    public GameObject pickupEffect;

    private GameObject Player;
    private PlayerController playerScript;

    private SFXManager SFXM;
    private GameManager GM;

    private SpriteRenderer playerSR;

    float cduration = 5f; // duration in seconds

    private float t = 0; // lerp control variable

    public Color playerBlankColor = Color.white;
    private bool playerIsFlashing;

    private CrazyColorChange CCC;

    private Animator anim;

    private void Start()
    {
        Player = GameObject.FindWithTag("Player");

        playerScript = FindObjectOfType<PlayerController>();

        SFXM = FindObjectOfType<SFXManager>();

        GM = FindObjectOfType<GameManager>();

        playerSR = Player.GetComponent<SpriteRenderer>();

        CCC = FindObjectOfType<CrazyColorChange>();

        anim = FindObjectOfType<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            StartCoroutine(PickupInvicible(other));

        }
    }

    IEnumerator PickupInvicible(Collider2D player)
    {

        InvicibleParticleEffect = Instantiate(pickupEffect, player.transform.position, transform.rotation);
        InvicibleParticleEffect.transform.parent = player.transform;

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        player.GetComponent<CircleCollider2D>().enabled = false;
        player.GetComponentInChildren<PolygonCollider2D>().enabled = true;

        CCC.isCrazyColorChanging = true;

        anim.SetBool("Blank", true);

        yield return new WaitForSeconds(duration);

        Destroy(InvicibleParticleEffect);

        

        playerIsFlashing = true;

        yield return new WaitForSeconds(2f);

        playerSR.color = playerBlankColor;

        playerIsFlashing = false;

        anim.SetBool("Blank", false);
        CCC.isCrazyColorChanging = false;
        

        player.GetComponent<CircleCollider2D>().enabled = true;

        Destroy(gameObject);

    }


 
    private void PlayerFlash()
    {
        Color P = playerSR.color;
        P.a = Mathf.Cos(30 * GM.T);
        playerSR.color = P;
    }

    private void Update()
    {
        if (playerIsFlashing)
        {
            PlayerFlash();
        }
    }
}