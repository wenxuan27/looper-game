﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawning : MonoBehaviour
{

    public GameObject player;

    public GameObject obstacle;
    public float amountOfObstacles;

    public float minX, maxX;


    //[SerializeField]
    //private Obstacle trianglePrefab;



    void Start()
    {
        player = GameObject.FindWithTag("Player");

        

        for (int i = 0; i < amountOfObstacles; i++)
        {
            float xAXIS, yAXIS;
            xAXIS = Random.Range(minX, maxX);
   
                yAXIS = Random.Range(player.transform.localPosition.y + 10, player.transform.localPosition.y + 50);

            Vector2 pos = new Vector2(xAXIS, yAXIS);
            Instantiate(obstacle, pos, obstacle.transform.rotation);
        }
    }

    /*public bool randomBoolean()
    {
        return (Random.Range(0f, 1f) >= 0.5f);
    }
    */
    


}
