﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class PowerUpSpawning : MonoBehaviour
{

    private GameObject player;
    private Rigidbody2D playerRb;

    public GameObject Star;
    public GameObject Star2;
    public GameObject Star3;
    public GameObject CoinMagnetObject;

    public float amountOfPowerUps;

    public float minX, maxX;

    private GameObject start;

    private GameManager GM;

    private float sPositionY = 0;
    private float cPositionY;
    private float spawnInterval = 10;


    void Start()
    {
        player = GameObject.FindWithTag("Player");

        start = GameObject.FindWithTag("Start");

        GM = FindObjectOfType<GameManager>();

        playerRb = player.GetComponent<Rigidbody2D>();

        

        for (int i = 0; i < amountOfPowerUps; i++)
        {
            float xAXIS, yAXIS;
            xAXIS = Random.Range(minX, maxX);

            yAXIS = Random.Range(player.transform.localPosition.y + 10, player.transform.localPosition.y + 500);

            Vector2 pos = new Vector2(xAXIS, yAXIS);
            Instantiate(Star, pos, Quaternion.identity);
        }
    }


    private void Update()
    {

        cPositionY = player.transform.position.y;

        System.Random rnd = new System.Random();
        int starNesr = rnd.Next(1, 3);

        if (cPositionY - sPositionY >= spawnInterval)
        {

            if (RandomoutofnInt(10) == 5)
            {

                float xAXIS, yAXIS, starN;
                xAXIS = Random.Range(minX, maxX);

                yAXIS = Random.Range(player.transform.position.y + 10, player.transform.position.y + 10 + spawnInterval);
                Vector2 pos = new Vector2(xAXIS, yAXIS);

                starN = Random.Range(0, 3);

               // Debug.Log(pos);

                

                if (starN == 0)
                {
                    Instantiate(Star, pos, Star.transform.rotation);
                }
                else if(starN == 1)
                {
                    Instantiate(Star2, pos, Star.transform.rotation);
                }
                else if(starN == 2)
                {
                    Instantiate(Star3, pos, Star.transform.rotation);
                }

                Instantiate(CoinMagnetObject, pos, Star.transform.rotation);
            }
           // Debug.Log("C" + cPositionY);
           // Debug.Log("S" + sPositionY);
            sPositionY = cPositionY;
        }
    }

    public int RandomoutofnInt(int range)
    {
        System.Random r = new System.Random();
        int rInt = r.Next(0, range); //for ints
        //int range = 100;
        double rDouble = r.NextDouble() * range; //for doubles
        return rInt;
    }

    public bool randomBoolean()
    {
        return (UnityEngine.Random.Range(0f, 1f) >= 0.5f);
    }


}
