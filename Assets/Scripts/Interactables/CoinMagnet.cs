﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMagnet : MonoBehaviour
{

    public float duration = 4f;

   

    private GameObject explodeParticleEffect;
    public GameObject explodepickupEffect;

    private GameObject Player;
    private PlayerController playerScript;

    private SFXManager SFXM;
    private GameManager GM;

    static float OldMoveSpeed;

    private SpriteRenderer playerSR;

    float cduration = 5f; // duration in seconds

    private float t = 0; // lerp control variable

    public Color playerBlankColor = Color.white;

    private bool playerIsFlashing;

    private CrazyColorChange CCC;

    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindWithTag("Player");

        playerScript = FindObjectOfType<PlayerController>();

        SFXM = FindObjectOfType<SFXManager>();

        GM = FindObjectOfType<GameManager>();

        playerSR = Player.GetComponent<SpriteRenderer>();

        CCC = FindObjectOfType<CrazyColorChange>();
        anim = FindObjectOfType<Animator>();


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("P") || other.tag == "Player")
        {

            StartCoroutine(CoinMagnetPickup(other));
            Debug.Log("coin Magnet");
        }
    }

    IEnumerator CoinMagnetPickup(Collider2D player)
    {
        explodeParticleEffect = Instantiate(explodepickupEffect, player.transform.position, transform.rotation);
        explodeParticleEffect.transform.parent = player.transform;

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        playerScript.cointMagnetOn = true;

        yield return new WaitForSeconds(duration);

        playerScript.cointMagnetOn = false;

        Destroy(gameObject);
    }

    private void PlayerFlash()
    {
        Color P = playerSR.color;
        P.a = Mathf.Cos(30 * GM.T);
        playerSR.color = P;
    }

    private void Update()
    {
        if (playerIsFlashing)
        {
            PlayerFlash();
        }
    }
}
