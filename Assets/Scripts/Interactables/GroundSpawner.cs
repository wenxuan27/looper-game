﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawner : MonoBehaviour
{

    public GameObject track;

    public bool spawned;

    [SerializeField]
    private float displacementY;

    private void OnTriggerEnter2D(Collider2D other)
        {

        

        if (other.gameObject.tag == "P" && !spawned)
        {
            Vector2 pos = new Vector2(transform.parent.position.x, (transform.parent.position.y + displacementY));
            Instantiate(track.transform, pos, transform.parent.rotation);
            spawned = true;

            
        }
    }


}