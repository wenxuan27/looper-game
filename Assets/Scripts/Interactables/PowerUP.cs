﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUP : MonoBehaviour
{

    public float duration = 3f;

    private GameObject turboParticleEffect;
    public GameObject pickupEffect;

    private GameObject explodeParticleEffect;
    public GameObject explodepickupEffect;

    private GameObject Player;
    private PlayerController playerScript;

    private SFXManager SFXM;
    private GameManager GM;

    static float OldMoveSpeed;

    private SpriteRenderer playerSR;

    float cduration = 5f; // duration in seconds

    private float t = 0; // lerp control variable

    public Color playerBlankColor = Color.white;

    private bool playerIsFlashing;

    private CrazyColorChange CCC;

    private Animator anim;

    private void Start()
    {
        Player = GameObject.FindWithTag("Player");

        playerScript = FindObjectOfType<PlayerController>();

        SFXM = FindObjectOfType<SFXManager>();

        GM = FindObjectOfType<GameManager>();

        playerSR = Player.GetComponent<SpriteRenderer>();

        CCC = FindObjectOfType<CrazyColorChange>();
        anim = FindObjectOfType<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            StartCoroutine(PickupSpeed(other));

        }
    }

    IEnumerator PickupSpeed(Collider2D player)
    {

        turboParticleEffect = Instantiate(pickupEffect, player.transform.position, transform.rotation);
        turboParticleEffect.transform.parent = player.transform;

        explodeParticleEffect = Instantiate(explodepickupEffect, player.transform.position, transform.rotation);
        explodeParticleEffect.transform.parent = player.transform;

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        player.GetComponent<CircleCollider2D>().enabled = false;
        player.GetComponentInChildren<PolygonCollider2D>().enabled = true;

        GM.Turbo = true;

       //UpdateplayerSR(1f, playerBlankColor, "red", null);
        CCC.isCrazyColorChanging = true;
        anim.SetBool("Blank", true);   

        yield return new WaitForSeconds(duration);

        GM.Turbo = false;

        Destroy(turboParticleEffect);

        playerIsFlashing = true;

        yield return new WaitForSeconds(2f);

        playerSR.color = playerBlankColor;
        anim.SetBool("Blank", false);

        CCC.isCrazyColorChanging = false;
        
        playerIsFlashing = false;

        player.GetComponent<CircleCollider2D>().enabled = true;

        Destroy(gameObject);

    }


    public void UpdateplayerSR(float transitionTime, Color fadeColor, string c, Action func)
    {
       // playerSR.color = fadeColor;
        StartCoroutine(UpdateplayerSRcolor(transitionTime, c, func));
    }

    private IEnumerator UpdateplayerSRcolor(float transitionTime, string color, Action func)
    {
        float t = 0.0f;

        for (t = 0.0f; t <= 1; t += Time.deltaTime / transitionTime)
        {
            if (color == "red")
            {

                //playerSR.color = new Color(t, SR.color.g, SR.color.b);

                playerSR.color = Color.Lerp(Color.white, Color.red, t);

            }

            if (color == "green")
            {
                //playerSR.color = new Color(0, t, 0);
                playerSR.color = Color.Lerp(Color.white, Color.green, t);
            }

            if (color == "blue")
            {
                //playerSR.color = new Color(0, 0, t);
                playerSR.color = Color.Lerp(Color.white, Color.blue, t);
            }

            if(color == "white")
            {
                //playerSR.color = new Color(t, t, t);
                playerSR.color = Color.Lerp(Color.red, Color.white, t);
            }

            if (color == "yellow")
            {
                //playerSR.color = new Color(t, t, 0);
                playerSR.color = Color.Lerp(Color.white, Color.yellow, t);
            }

            yield return null;


        }

        //SR.color.a = 1;

        func?.Invoke();
    }

    

    private void PlayerFlash()
    {
        Color P = playerSR.color;
        P.a = Mathf.Cos(30 * GM.T);
        playerSR.color = P;
    }

    private void Update()
    {
        if (playerIsFlashing)
        {
            PlayerFlash();
        }
    }
}
